#!/usr/bin/env python3

import os
import pprint
import typing
from argparse import ArgumentParser
from argparse import Namespace
import inspect

import yaml

from core.exceptions import NoSuchSymbolError
from core.exceptions import ParseError
from core import stdlib

__VERSION__ = "0.0.1"

def parse_args() -> Namespace:
    """
    Parse arguments and return Namespace.
    """
    argp = ArgumentParser(prog='yamllang')
    argp.add_argument('fpath',
                      help='Path to YAMLLang file to execute')
    argp.add_argument('-v', '--version',
                      help='Show version number and exit',
                      action='store_true')
    argp.add_argument('--verbose',
                      help='Run in verbose mode',
                      action='store_true')

    args = argp.parse_args()

    if not os.path.exists(args.fpath):
        raise ValueError('Cannot find {}'.format(args.fpath))

    return args

def validate_file(args: Namespace) -> dict:
    """
    Validate the YAMLLang file and return the prepared dictionary.
    """
    try:
        prog = yaml.safe_load(open(args.fpath))
    except yaml.scanner.ScannerError as sad:
        if args.verbose:
            msg = "Failed to parse {pth}, caused by {ex}".format(pth=args.fpath,
                                                                 ex=type(sad))
            raise ParseError(sad, msg)
        else:
            msg = 'Failed to parse {pth}'.format(pth=args.fpath)
            raise ParseError(sad, msg) from None

    return prog

def prepare_context(context: dict) -> dict:
    """
    Prepare new context object.
    """
    return {
        '__OUTER__': context
    }

def find_symbol(symbol: str, context: dict) -> typing.Any:
    """
    Find symbol in context or parents.
    """
    if symbol in context:
        return context.get(symbol)

    if context.get('__OUTER__') is None:
        return None

    return find_symbol(symbol, context.get('__OUTER__'))

def execute(block: typing.Any,
            context: dict) -> None:
    """
    Execute PROG, given ARGS and CONTEXT
    """
    if find_symbol('__ARGS__', context).verbose:
        print('Executing:')
        pprint.pprint(block)
        print('Context:')
        pprint.pprint(context)
    # Handle function body execution
    if isinstance(block, list):
        # Lists are bodies, and should executed statement-by-statement
        for stmt in block:
            execute(stmt, prepare_context(context))
    elif isinstance(block, dict):
        # Dicts are function calls or keywords
        # First, look at keywords
        symbol_name = list(block.keys())[0]
        if symbol_name == 'return':
            return execute(block.get(symbol_name)[0],
                           prepare_context(context))
        # It's a function
        resolved_symbol = find_symbol(symbol_name, context)
        if resolved_symbol is None:
            raise NoSuchSymbolError(symbol_name, context)

        # _func is a Python function
        if symbol_name[0] == '_':
            args = [execute(x, prepare_context(context))
                    for x in block.get(symbol_name)]
            ret = resolved_symbol(args)
        else:
            ret = execute(resolved_symbol,
                          prepare_context(context))
        return ret
    elif isinstance(block, str):
        # Strings are either function calls without parameters, or variables
        resolved_symbol = find_symbol(block, context)
        if resolved_symbol is None:
            return block
        ret = execute(resolved_symbol, prepare_context(context))
        return ret
    else:
        # Just return everything else
        return block

def init_context(args: dict, prog: dict) -> dict:
    """
    Initialize context with any user-defined functions.
    """
    std_funcs = {
        k[0]: k[1]
        for k in inspect.getmembers(stdlib, inspect.isfunction)
    }

    funcs = {
        k: prog.get(k)
        for k in prog
        if k != 'main'
    }

    return {
        '__ARGS__': args,
        '__OUTER__': None,
        **funcs,
        **std_funcs
    }

if __name__ == '__main__':
    ARGS = parse_args()

    if ARGS.version:
        print(__VERSION__)
        exit(0)

    try:
        PROG = validate_file(ARGS)
    except ParseError as sadness:
        print(sadness.msg)
        exit(1)

    CONTEXT = init_context(ARGS, PROG)

    if 'main' not in PROG:
        raise NoSuchSymbolError('main', CONTEXT)

    RET = execute(PROG.get('main'), CONTEXT)
    exit(RET)
