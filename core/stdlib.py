"""
Core functions of YAMLLang.
"""
import typing

def _print(string: list) -> None:
    """
    Provide print functionality.
    """
    print(string[0])

def _add(lst: list) -> typing.Union[int, float]:
    """
    Sum arguments
    """
    return sum(lst)

def _foobar() -> None:
    """
    No-args debugging function.
    """
    print('Foobar!')

def _sub(args: list) -> typing.Union[int, float]:
    """
    Subtract two numbers, return result.
    """
    return args[0] - args[1]
