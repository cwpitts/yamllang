"""
Exceptions for YAMLLang.
"""
from copy import deepcopy

from editdistance import eval as dist

class YAMLLangError(Exception):
    """
    Generic base class for exceptions.
    """
    pass

class ParseError(YAMLLangError):
    """
    ParseError is thrown when parsing fails (obviously).
    """
    def __init__(self, exception, msg):
        self.exception = exception
        self.msg = msg
        super().__init__()

    def __repr__(self):
        return self.msg

class NoSuchSymbolError(YAMLLangError):
    """
    NoSuchSymbolError is thrown when a symbol is referenced without being defined.
    """
    def __init__(self, method: str, context: dict):
        self.method = method
        self.context = deepcopy(context)
        super().__init__()

    def _find_all_symbols(self, context: dict) -> list:
        """
        Iterate upwards through the context, finding all symbols.
        """
        symbols = [x for x in context
                   if not x.startswith('__')]
        if context.get('__OUTER__') is not None:
            return symbols + self._find_all_symbols(context.get('__OUTER__'))
        return symbols

    def __repr__(self):
        return self.method

    def __str__(self):
        all_symbols = self._find_all_symbols(self.context)
        symbol_distance = [x for x in sorted([(x, dist(self.method, x))
                                              for x in all_symbols],
                                             key=lambda x: x[1])
                           if x[1] <= 3]
        msg = self.method + ' not found in context.'
        if not symbol_distance:
            msg += "There wasn't anything even close to suggest, by the way."
        else:
            msg += ' Suggestions:'
            for symbol in symbol_distance:
                msg += '\n\t{sym}'.format(sym=symbol[0])
        return msg
